 * This module is used to show allowed links in iFrame.
 * Used to show all internal links, and external links(where iframe allowed)
 * "X-Frame-Options for given URL is Denied to show in iFrame" error if X-Frame is false in headers.
